package com.geet.word_identifier.searching;

public class Word {

	private String	written ="",actual ="",predict="";
	private Word(){}
	
	public static class Builder{
		private String	written ="",actual ="",predict="";
		public Builder writtenWord(String word){
			written = word;
			return this;
		}
		public Builder actualWord(String word){
			actual = word;
			return this;
		}
		public Builder predictWord(String word){
			predict = word;
			return this;
		}
		
		public Word build(){
			Word word = new Word();
			word.setWritten(written);
			word.setActual(actual);
			word.setPredict(predict);
			return word;
		}
	}

	public String getWritten() {
		return written;
	}

	public void setWritten(String written) {
		this.written = written.toLowerCase();
	}

	public String getActual() {
		return actual;
	}

	public void setActual(String actual) {
		this.actual = actual.toLowerCase();
	}

	public String getPredict() {
		return predict;
	}

	public void setPredict(String predict) {
		this.predict = predict.toLowerCase();
	}
	static String newLine = "\n";
	public String toXMLString(){
		return "<word>"+newLine+
				"<written>"+written+"</written>"+newLine+
				"<actual>"+actual+"</actual>"+newLine+
				"<predict>"+predict+"</predict>"+newLine+
				"</word>";
	}
	
}
