package com.geet.word_identifier.searching;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.geet.word_identifier.corpus.SimpleWord;
import com.geet.word_identifier.indexing_vsm.EditDistance;
import com.geet.word_identifier.indexing_vsm.Query;
import com.geet.word_identifier.indexing_vsm.VectorSpaceMatrix;
import com.geet.word_identifier.indexing_vsm.VectorSpaceModel;
import com.geet.word_identifier.io.JavaFileReader;
import com.geet.word_identifier.model.Area;
import com.geet.word_identifier.model.Problem;
import com.geet.word_identifier.model.ProblemTrain;
import com.geet.word_identifier.model.RoadAndTransport;
import com.geet.word_identifier.model.WaterAndSupply;

public class Run {
	static String newLine = "\n";
	public static void main(String[] args) throws Exception {
		generateAndStoreVectorSpaceMatrix();
	}
	private static Problem predictWithEditDistance(Problem problem){
		VectorSpaceModel vectorSpaceModel;
		try {
			vectorSpaceModel = new VectorSpaceModel(
					loadVectorSpaceMatrix());
			List<SimpleWord> dictionaryWords = vectorSpaceModel
					.getVectorSpaceMatrix().getWords();
			StringTokenizer stringTokenizer = new StringTokenizer(
					problem.getDescription(), " ,;#", false);
			boolean area = false, type = false;
			System.out.println(problem.getDescription());
			while (stringTokenizer.hasMoreTokens()) {
				if (area && type) {
					break;
				}
				String token = stringTokenizer.nextToken();
				List<SimpleWord> candidateWords = new ArrayList<SimpleWord>();
				for (int i = 0; i < dictionaryWords.size(); i++) {
					double score = EditDistance.naiveEditDistance(token
							.toLowerCase(), dictionaryWords.get(i)
							.getWordInString());
					if (score <= 2) {
						candidateWords.add(new SimpleWord.Builder()
								.word(dictionaryWords.get(i).getWordInString())
								.score(score).build());
					}
				}
				Collections.sort(candidateWords);
				if (candidateWords.size() > 0) {
					if (!area) {
						if (Area.isInDictionary(candidateWords.get(0)
								.getWordInString())) {
							problem.setArea(candidateWords.get(0).getWordInString());
							area = true;
						}
					}
					if (!type) {
						if (RoadAndTransport.isInDictionary(candidateWords.get(0)
								.getWordInString())) {
							problem.setType(new RoadAndTransport().toString());
							type = true;
						} else if (WaterAndSupply.isInDictionary(candidateWords
								.get(0).getWordInString())) {
							problem.setType(new WaterAndSupply().toString());
							type = true;
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return problem;
	}
	private static void predictAreasWithEditDistance(){
		List<ProblemTrain> problems = new ArrayList<ProblemTrain>();
		try {
			problems = readProblemsFromXML("src/res/problems.xml");
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
			List<SimpleWord> dictionaryWords = vectorSpaceModel.getVectorSpaceMatrix().getWords();
			FileWriter fileWriter = new FileWriter("src/res/output_edit_area.xml");
			fileWriter.write("<problems>"+newLine);
			for (ProblemTrain problem:problems){
				StringTokenizer stringTokenizer = new StringTokenizer(problem.getDescription()," ,;#",false);
				boolean area = false, type = false;
				System.out.println(problem.getDescription());
				while (stringTokenizer.hasMoreTokens()) {
					if (area && type) {
						break;
					}
					String token = stringTokenizer.nextToken();
					List<SimpleWord> candidateWords = new ArrayList<SimpleWord>(); 
					for (int i = 0; i < dictionaryWords.size(); i++) {
						double score = EditDistance.naiveEditDistance(token.toLowerCase(), dictionaryWords.get(i).getWordInString());
						if (score <= 2) {
							candidateWords.add(new SimpleWord.Builder().word(dictionaryWords.get(i).getWordInString()).score(score).build());
						}
					}
					Collections.sort(candidateWords);
					if (candidateWords.size()> 0) {
						if (!area) {
							if (Area.isInDictionary(candidateWords.get(0).getWordInString())) {
								problem.setArea_predict(candidateWords.get(0).getWordInString());
								area = true;
							}
						}
						if (!type) {
							if(RoadAndTransport.isInDictionary(candidateWords.get(0).getWordInString())){
								problem.setType_predict(new RoadAndTransport().toString());	
								type = true;
							}else if(WaterAndSupply.isInDictionary(candidateWords.get(0).getWordInString())){
								problem.setType_predict(new WaterAndSupply().toString());
								type = true;
							}
						}
					}
				}
				fileWriter.write(problem.toXMLString()+"\n");
			}
			fileWriter.write("</problems>");
			fileWriter.close();
			System.out.println("OKAY EDIT DISTANCE");
		} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return ;
	}
	private static List<Word> outputWithEditDistance(){
		List<Word> words = new ArrayList<Word>();
		try {
			words = readWords("src/res/words.xml");
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
			List<SimpleWord> dictionaryWords = vectorSpaceModel.getVectorSpaceMatrix().getWords();
			FileWriter fileWriter = new FileWriter("src/res/output_edit.xml");
			fileWriter.write("<words>"+newLine);
			for (Word word:words){
				List<SimpleWord> candidateWords = new ArrayList<SimpleWord>(); 
				for (int i = 0; i < dictionaryWords.size(); i++) {
					double score = EditDistance.naiveEditDistance(word.getWritten().toLowerCase(), dictionaryWords.get(i).getWordInString());
					score = score / (double) dictionaryWords.get(i).getWordInString().length();
					if (score <= 0.7) {
						candidateWords.add(new SimpleWord.Builder().word(dictionaryWords.get(i).getWordInString()).score(score).build());
					}
				}
				Collections.sort(candidateWords);
				if (candidateWords.size()> 0) {
					word.setPredict(candidateWords.get(0).getWordInString());
				}else{
					word.setPredict("EOF");
				}
				fileWriter.write(word.toXMLString()+newLine);
			}
			fileWriter.write("</words>");
			fileWriter.close();
			System.out.println("OKAY EDIT DISTANCE");
		} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return words;
					
	}
	private static Problem  predictWithVectorSpaceModel(Problem problem){
		VectorSpaceModel vectorSpaceModel;
			try {
				vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
				StringTokenizer stringTokenizer = new StringTokenizer(problem.getDescription()," ,;#",false);
				System.out.println(problem.getDescription());
				boolean area = false, type = false;
				while (stringTokenizer.hasMoreTokens()) {
					if (area && type) {
						break;
					}
					String token = stringTokenizer.nextToken();
					Query query = vectorSpaceModel.getQuery(new SimpleWord.Builder().word(token.toLowerCase()).build());
					List<SimpleWord> retWords = vectorSpaceModel.searchWithQueryVector(query);
					if (retWords.size()>0) {
						if (!area) {
							if (Area.isInDictionary(retWords.get(0).getWordInString())) {
								problem.setArea(retWords.get(0).getWordInString());
								area = true;
							}
						}
						if (!type) {
							if(RoadAndTransport.isInDictionary(retWords.get(0).getWordInString())){
								problem.setType(new RoadAndTransport().toString());	
								type = true;
							}else if(WaterAndSupply.isInDictionary(retWords.get(0).getWordInString())){
								problem.setType(new WaterAndSupply().toString());
								type = true;
							}
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return problem;
	}
	private static void predictAreasWithVectorSpaceModel(){
		List<ProblemTrain> problems = new ArrayList<ProblemTrain>();
		try {
			problems = readProblemsFromXML("src/res/problems.xml");
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
			FileWriter fileWriter = new FileWriter("src/res/output_vsm_area.xml");
			fileWriter.write("<problems>"+newLine);
			for (ProblemTrain problem:problems){
				StringTokenizer stringTokenizer = new StringTokenizer(problem.getDescription()," ,;#",false);
				System.out.println(problem.getDescription());
				boolean area = false, type = false;
				while (stringTokenizer.hasMoreTokens()) {
					if (area && type) {
						break;
					}
					String token = stringTokenizer.nextToken();
					Query query = vectorSpaceModel.getQuery(new SimpleWord.Builder().word(token.toLowerCase()).build());
					List<SimpleWord> retWords = vectorSpaceModel.searchWithQueryVector(query);
					if (retWords.size()>0) {
						if (!area) {
							if (Area.isInDictionary(retWords.get(0).getWordInString())) {
								problem.setArea_predict(retWords.get(0).getWordInString());
								area = true;
							}
						}
						if (!type) {
							if(RoadAndTransport.isInDictionary(retWords.get(0).getWordInString())){
								problem.setType_predict(new RoadAndTransport().toString());	
								type = true;
							}else if(WaterAndSupply.isInDictionary(retWords.get(0).getWordInString())){
								problem.setType_predict(new WaterAndSupply().toString());
								type = true;
							}
						}
					}
				}
				System.out.println(problem.toXMLString());
				fileWriter.write(problem.toXMLString()+"\n");
			}
			fileWriter.write("</problems>");
			fileWriter.close();
			System.out.println("OKAY Vector Space Modeling done");
		} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return ;

	}
	private static List<Word> outputWithVectorSpaceModel(){
		List<Word> words = new ArrayList<Word>();
		try {
			words = readWords("src/res/words.xml");
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
			FileWriter fileWriter = new FileWriter("src/res/output_vsm.xml");
			fileWriter.write("<words>"+newLine);
			for (Word word:words){
				Query query = vectorSpaceModel.getQuery(new SimpleWord.Builder().word(word.getWritten().toLowerCase()).build());
				List<SimpleWord> retWords = vectorSpaceModel.searchWithQueryVector(query);
				if (retWords.size()>0) {
					word.setPredict(retWords.get(0).getWordInString());
				}else{
					word.setPredict("EOF");
				}
				fileWriter.write(word.toXMLString()+newLine);
			}
			fileWriter.write("</words>");
			fileWriter.close();
			} catch (IOException e) {
			e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return words;
	}
	private static List<Word> outputWithVectorSpaceModelAndEditDistance(){
		List<Word> words = new ArrayList<Word>();
		try {
			words = readWords("src/res/words.xml");
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(
					loadVectorSpaceMatrix());
			FileWriter fileWriter = new FileWriter(
					"src/res/output_vsm_edit.xml");
			fileWriter.write("<words>" + newLine);
			for (Word word : words) {
				Query query = vectorSpaceModel
						.getQuery(new SimpleWord.Builder().word(
								word.getWritten().toLowerCase()).build());
				List<SimpleWord> retWords = vectorSpaceModel
						.searchWithQueryVector(query);
				for (SimpleWord retWord : retWords) {
					retWord.setScore(1.0);
					double score = EditDistance.naiveEditDistance(
							word.getWritten().toLowerCase(),
							retWord.getWordInString());
					score = score / (double) retWord.getWordInString().length();
					if (score <= 0.7) {
						retWord.setScore(score);
					}
					break;
				}
				Collections.sort(retWords);
				if (retWords.size() > 0) {
					word.setPredict(retWords.get(0).getWordInString());
				} else {
					word.setPredict("EOF");
				}
				fileWriter.write(word.toXMLString() + newLine);
			}
			fileWriter.write("</words>");
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return words;
	}
	
	private static void predictAreasWithVectorSpaceModelAndEditDistance(){
		List<ProblemTrain> problems = new ArrayList<ProblemTrain>();
		try {
			problems = readProblemsFromXML("src/res/problems.xml");
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
			FileWriter fileWriter = new FileWriter("src/res/output_vsm_edit_area.xml");
			fileWriter.write("<problems>"+newLine);
			for (ProblemTrain problem:problems){
				StringTokenizer stringTokenizer = new StringTokenizer(problem.getDescription()," ,;#",false);
				System.out.println(problem.getDescription());
				boolean area = false, type = false;
				while (stringTokenizer.hasMoreTokens()) {
					if (area && type) {
						break;
					}
					String token = stringTokenizer.nextToken();
					Query query = vectorSpaceModel.getQuery(new SimpleWord.Builder().word(token.toLowerCase()).build());
					List<SimpleWord> retWords = vectorSpaceModel.searchWithQueryVector(query);
					List<SimpleWord> refinedWords = new ArrayList<SimpleWord>(); 
					for (int i = 0; i < retWords.size(); i++) {
						double score = EditDistance.naiveEditDistance(token.toLowerCase(), retWords.get(i).getWordInString());
						if (score <= 2) {
							refinedWords.add(retWords.get(i));
						}
					}
					if (refinedWords.size()>0) {
							if (!area) {
								if (Area.isInDictionary(refinedWords.get(0).getWordInString())) {
									problem.setArea_predict(refinedWords.get(0).getWordInString());
									area = true;
								}
							}
							if (!type) {
								if(RoadAndTransport.isInDictionary(refinedWords.get(0).getWordInString())){
									problem.setType_predict(new RoadAndTransport().toString());	
									type = true;
								}else if(WaterAndSupply.isInDictionary(refinedWords.get(0).getWordInString())){
									problem.setType_predict(new WaterAndSupply().toString());
									type = true;
								}
							}
					}
				}
				fileWriter.write(problem.toXMLString()+"\n");
			}
			fileWriter.write("</problems>");
			fileWriter.close();
			System.out.println("OKAY Vector Space Modeling and Edit Distance done");
		} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return ;

	}
	private static Problem predictWithVectorSpaceModelAndEditDistance(Problem problem){
			VectorSpaceModel vectorSpaceModel;
			try {
				vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
				StringTokenizer stringTokenizer = new StringTokenizer(problem.getDescription()," ,;#",false);
				System.out.println(problem.getDescription());
				boolean area = false, type = false;
				while (stringTokenizer.hasMoreTokens()) {
					if (area && type) {
						break;
					}
					String token = stringTokenizer.nextToken();
					Query query = vectorSpaceModel.getQuery(new SimpleWord.Builder().word(token.toLowerCase()).build());
					List<SimpleWord> retWords = vectorSpaceModel.searchWithQueryVector(query);
					List<SimpleWord> refinedWords = new ArrayList<SimpleWord>(); 
					for (int i = 0; i < retWords.size(); i++) {
						double score = EditDistance.naiveEditDistance(token.toLowerCase(), retWords.get(i).getWordInString());
						if (score <= 2) {
							refinedWords.add(retWords.get(i));
						}
					}
					if (refinedWords.size()>0) {
							if (!area) {
								if (Area.isInDictionary(refinedWords.get(0).getWordInString())) {
									problem.setArea(refinedWords.get(0).getWordInString());
									area = true;
								}
							}
							if (!type) {
								if(RoadAndTransport.isInDictionary(refinedWords.get(0).getWordInString())){
									problem.setType(new RoadAndTransport().toString());	
									type = true;
								}else if(WaterAndSupply.isInDictionary(refinedWords.get(0).getWordInString())){
									problem.setType(new WaterAndSupply().toString());
									type = true;
								}
							}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		return problem;
	}
	public static List<ProblemTrain> readProblemsFromXML(String filePath) throws Exception{
		List<ProblemTrain> problems = new ArrayList<ProblemTrain>();
		File inputFile = new File(filePath);
        DocumentBuilderFactory dbFactory 
           = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        org.w3c.dom.Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        /*System.out.println("Root element :" 
           + doc.getDocumentElement().getNodeName());
        */NodeList nList = doc.getElementsByTagName("problem");
        //System.out.println("----------------------------");
        problems = new ArrayList<ProblemTrain>();
		for(int i=0;i<nList.getLength();i++)
	    {
	        Element wordElement = (Element) nList.item(i);
	        String des = wordElement.getElementsByTagName("des").item(0).getTextContent();
	        String area = wordElement.getElementsByTagName("area_actual").item(0).getTextContent();
	        String type = wordElement.getElementsByTagName("type_actual").item(0).getTextContent();
	        String area_predict = wordElement.getElementsByTagName("area_predict").item(0).getTextContent();
	        String type_predict = wordElement.getElementsByTagName("type_predict").item(0).getTextContent();
	        ProblemTrain problem = new ProblemTrain.ProblemBuiler().description(des).area(area).type(type).area_predict(area_predict).type_predict(type_predict).build();
	      //  System.out.println(problem.toXMLString());
	        problems.add(problem);
	    }
		return problems;
	}
	public static List<Problem> readProblemsFromTXTAndPredictWithVSM(String filePath) throws Exception{
		List<Problem> problems = new ArrayList<Problem>();
		File inputFile = new File(filePath);
		JavaFileReader javaFileReader = new JavaFileReader();
		if (javaFileReader.openFile(inputFile)) {
			String text = javaFileReader.getText();
			Scanner scanner = new Scanner(text);
			while (scanner.hasNext()) {
				Problem problem = new Problem.ProblemBuiler().description(scanner.nextLine()).build();
				problem = predictWithVectorSpaceModel(problem);
				problems.add(problem);
			}
			scanner.close();
		}
		return problems;
	}

	public static List<Word> readWords(String filePath) throws Exception{
		List<Word> words = new ArrayList<Word>();
		File inputFile = new File(filePath);
        DocumentBuilderFactory dbFactory 
           = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        org.w3c.dom.Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        /*System.out.println("Root element :" 
           + doc.getDocumentElement().getNodeName());
        */NodeList nList = doc.getElementsByTagName("word");
        //System.out.println("----------------------------");
        words = new ArrayList<Word>();
		for(int i=0;i<nList.getLength();i++)
	    {
	        Element wordElement = (Element) nList.item(i);
	        String writtenWord = wordElement.getElementsByTagName("written").item(0).getTextContent();
	        String actualWord = wordElement.getElementsByTagName("actual").item(0).getTextContent();
	        String predictedWord = wordElement.getElementsByTagName("predict").item(0).getTextContent();
	        Word word = new Word.Builder().writtenWord(writtenWord).actualWord(actualWord).predictWord(predictedWord).build();
	//        System.out.println(word.getWritten()+" "+ word.getActual()+" "+word.getPredict());
	        words.add(word);
	    }
		return words;
	}
	private static void compareResult(String filePath){
		try {
			List<Word> words = readWords(filePath);
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
			List<SimpleWord> dictionaryWords = vectorSpaceModel.getVectorSpaceMatrix().getWords();
			int right = 0, wrong = 0;
			for (Word word : words) {
				boolean isActualWordPresentInDictionary = false;
				for (SimpleWord dictionaryWord : dictionaryWords) {
					if (dictionaryWord.getWordInString().equals(word.getActual())) {
						isActualWordPresentInDictionary= true;
						break;
					}
				}
				if (isActualWordPresentInDictionary) {
					if (word.getActual().equals(word.getPredict())) {
						right ++;
					}else {
						System.out.println(word.getWritten()+","+word.getActual()+","+word.getPredict()+"0");
						wrong++;
					}					
				}
				else{
					if (word.getPredict().equals("eof")) {
						right++;
					}else{
						System.out.println(word.getWritten()+","+word.getActual()+","+word.getPredict()+"0");
						wrong++;
					}
				}
			}
			System.out.println("Right :"+right+"Wrong :"+wrong);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	private static void processDataInXMLFile(){
		try {
			File inputFile = new File("src/res/word.txt");
			FileWriter fileWriter = new FileWriter("src/res/words.xml");
			Scanner scanner = new Scanner(inputFile);
			List<String> areas = new ArrayList<String>();
			
			fileWriter.write("<words>" + newLine);
			while (scanner.hasNext()) {
				String text = scanner.nextLine();
				StringTokenizer stringTokenizer = new StringTokenizer(text,
						" ,.#;\n", false);
				System.out.println(stringTokenizer.countTokens());

				while (stringTokenizer.hasMoreTokens()) {
					String token = stringTokenizer.nextToken();
					System.out.println(token);
					token = token.toLowerCase();
					fileWriter.write("<word>" + newLine);
					fileWriter.write("<written>" + token + "</written>"
							+ newLine);
					fileWriter.write("<actual>" + "" + "</actual>" + newLine);
					fileWriter.write("<predict>" + "" + "</predict>" + newLine);
					fileWriter.write("</word>" + newLine);
				}
			}
			fileWriter.write("</words>" + newLine);
			fileWriter.close();
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void generateAndStoreVectorSpaceMatrix(){
		try {
			Scanner scanner = new Scanner(new File("src/res/dictionary.txt"));
			List<SimpleWord> simpleWords = new ArrayList<SimpleWord>();
			while (scanner.hasNext()) {
				String token = scanner.nextLine();
				token = token.toLowerCase();
				simpleWords.add(new SimpleWord.Builder().word(token).build());
			}
			scanner.close();
			System.out.println(simpleWords.size());
			for (SimpleWord simpleWord : simpleWords) {
				System.out.println(simpleWord.getWordInString());
			}
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(simpleWords);
			storeVectorSpaceMatrix(vectorSpaceModel.getVectorSpaceMatrix());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	public static void storeVectorSpaceMatrix(VectorSpaceMatrix vectorSpaceMatrix){
		try {
			FileOutputStream file = new FileOutputStream("vectorspace.ser");
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(file);
			objectOutputStream.writeObject(vectorSpaceMatrix);
			objectOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static VectorSpaceMatrix loadVectorSpaceMatrix() throws IOException{
		VectorSpaceMatrix vectorSpaceMatrix = null;
		FileInputStream file = new FileInputStream("vectorspace.ser");
		ObjectInputStream objectInputStream = new ObjectInputStream(file);
		try {
			vectorSpaceMatrix = (VectorSpaceMatrix) objectInputStream.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		objectInputStream.close();
		return vectorSpaceMatrix;
	}
	private static void compareResultArea(String filePath){
		try {
			List<ProblemTrain> problems = readProblemsFromXML(filePath);
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
			List<SimpleWord> dictionaryWords = vectorSpaceModel.getVectorSpaceMatrix().getWords();
			int right = 0, wrong = 0;
			for (ProblemTrain problem : problems) {
				boolean isActualWordPresentInDictionary = false;
				for (SimpleWord dictionaryWord : dictionaryWords) {
					if (dictionaryWord.getWordInString().equals(problem.getArea())) {
						isActualWordPresentInDictionary= true;
						break;
					}
				}
				if (isActualWordPresentInDictionary) {
					if (problem.getArea().equals(problem.getArea_predict())) {
						right ++;
					}else {
					//	System.out.println(problem.toXMLString());
						wrong++;
					}					
				}
				else{
					if (problem.getArea_predict().equals("")) {
						right++;
					}else{
					//	System.out.println(problem.toXMLString());
						wrong++;
					}
				}
			}
			System.out.println("Right :"+right+"Wrong :"+wrong);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	private static void compareResultType(String filePath){
		try {
			List<ProblemTrain> problems = readProblemsFromXML(filePath);
			VectorSpaceModel vectorSpaceModel = new VectorSpaceModel(loadVectorSpaceMatrix());
			List<SimpleWord> dictionaryWords = vectorSpaceModel.getVectorSpaceMatrix().getWords();
			int right = 0, wrong = 0;
			for (ProblemTrain problem : problems) {
				boolean isActualWordPresentInDictionary = false;
				for (SimpleWord dictionaryWord : dictionaryWords) {
					if (dictionaryWord.getWordInString().equals(problem.getType())) {
						isActualWordPresentInDictionary= true;
						break;
					}
				}
				if (isActualWordPresentInDictionary) {
					if (problem.getType().equals(problem.getType_predict())) {
						right ++;
					}else {
					//	System.out.println(problem.toXMLString());
						wrong++;
					}					
				}
				else{
					if (problem.getType_predict().equals("")) {
						right++;
					}else{
					//	System.out.println(problem.toXMLString());
						wrong++;
					}
				}
			}
			System.out.println("Right :"+right+"Wrong :"+wrong);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
