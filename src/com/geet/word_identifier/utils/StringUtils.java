package com.geet.word_identifier.utils;

public class StringUtils {

	/**
	 * get the character index of ch in target starting from startIndex 
	 * @param ch
	 * @param startIndex
	 * @param target
	 * @return
	 */
	public static int getIndex(char ch, int startIndex,String target){
		for (int i = startIndex; i < target.length(); i++) {
			if (target.charAt(i) == ch) {
				return i;
			}
		}
		return -1;
	}
	public static void main(String[] args) {
		System.out.println(getIndex('c', 0, "dhanmondhi"));
	}
}
