package com.geet.word_identifier.model;

import com.geet.word_identifier.model.ProblemTrain.ProblemBuiler;

public class Problem {
	private String description="";
	private String area="";
	private String type="";
	public static class ProblemBuiler{
		private String description="";
		private String area="";
		private String type="";

		public ProblemBuiler description(String des){
			description = des;
			return this;
		}
		public ProblemBuiler area(String area){
			this.area = area;
			return this;
		}
		public ProblemBuiler type(String type){
			this.type = type;
			return this;
		}
		public Problem build(){
			Problem problem = new Problem();
			problem.setDescription(description);
			problem.setArea(area);
			problem.setType(type);
			return problem;
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String toXMLString() {
		String newLine = "\n";
		return "<problem>"+newLine+
				"<des>"+description+"</des>"+newLine+
				"<area_actual>"+area+"</area_actual>"+newLine+
				"<type_actual>"+type+"</type_actual>"+newLine+
				"</problem>";
	}

}
