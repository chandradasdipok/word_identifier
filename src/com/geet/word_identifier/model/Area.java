package com.geet.word_identifier.model;

public class Area {
	public static boolean isInDictionary(String area){
		if (area.equals(AreaType.hazaribag.toString())) {
			return true;
		}else if (area.equals(AreaType.dhanmondhi.toString())) {
			return true;
		}else if (area.equals(AreaType.jigatola.toString())) {
			return true;
		}else if (area.equals(AreaType.azimpur.toString())) {
			return true;
		}
		return false;
	}
	public enum AreaType {
		hazaribag,dhanmondhi,jigatola,azimpur
	}
	
	public static void main(String[] args) {
		System.out.println(AreaType.azimpur.toString());
	}

	public String[] getAreas(){
		String [] areas = new String[5];
		areas[0] = AreaType.hazaribag.toString().toUpperCase();
		areas[1] = AreaType.dhanmondhi.toString().toUpperCase();
		areas[2] = AreaType.jigatola.toString().toUpperCase();
		areas[3] = AreaType.azimpur.toString().toUpperCase();
		areas[4] = "Others".toUpperCase();
		return areas;
	}
}
