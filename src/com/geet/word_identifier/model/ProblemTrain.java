package com.geet.word_identifier.model;

public class ProblemTrain {

	private String description;
	private String area;
	private String type;
	private String area_predict;
	private String type_predict;
	
	public static class ProblemBuiler{
		private String description;
		private String area;
		private String type;
		private String area_predict;
		private String type_predict;

		public ProblemBuiler description(String des){
			description = des;
			return this;
		}
		public ProblemBuiler area(String area){
			this.area = area;
			return this;
		}
		public ProblemBuiler type(String type){
			this.type = type;
			return this;
		}
		public ProblemBuiler area_predict(String area){
			this.area_predict = area;
			return this;
		}
		public ProblemBuiler type_predict(String type){
			this.type_predict = type;
			return this;
		}
		
		public ProblemTrain build(){
			ProblemTrain problem = new ProblemTrain();
			problem.setDescription(description);
			problem.setArea(area);
			problem.setType(type);
			problem.setArea_predict(area_predict);
			problem.setType_predict(type_predict);
			return problem;
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getArea_predict() {
		return area_predict;
	}

	public void setArea_predict(String area_predict) {
		this.area_predict = area_predict;
	}

	public String getType_predict() {
		return type_predict;
	}

	public void setType_predict(String type_predict) {
		this.type_predict = type_predict;
	}

	public String toXMLString() {
		String newLine = "\n";
		return "<problem>"+newLine+
				"<des>"+description+"</des>"+newLine+
				"<area_actual>"+area+"</area_actual>"+newLine+
				"<area_predict>"+area_predict+"</area_predict>"+newLine+
				"<type_actual>"+type+"</type_actual>"+newLine+
				"<type_predict>"+type_predict+"</type_predict>"+newLine+
				"</problem>";
	}
	
}
