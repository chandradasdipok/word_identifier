package com.geet.word_identifier.model;

public class WaterAndSupply{
	public static boolean isInDictionary(String area){
		if (area.equals(WanterAndSupplyType.drainage.toString())) {
			return true;
		}else if (area.equals(WanterAndSupplyType.gas.toString())) {
			return true;
		}else if (area.equals(WanterAndSupplyType.electricity.toString())) {
			return true;
		}else if (area.equals(WanterAndSupplyType.pipeline.toString())) {
			return true;
		}else if (area.equals(WanterAndSupplyType.water.toString())) {
			return true;
		}else if (area.equals(WanterAndSupplyType.flood.toString())) {
			return true;
		}else if (area.equals(WanterAndSupplyType.overflow.toString())) {
			return true;
		}else if (area.equals(WanterAndSupplyType.drain.toString())) {
			return true;
		}
		return false;
	}
	public enum WanterAndSupplyType {
		drainage, gas,electricity, pipeline, water, flood, overflow, drain
	}

	@Override
	public String toString() {
		return "water";
	}
}
