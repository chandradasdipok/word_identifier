package com.geet.word_identifier.model;


public class RoadAndTransport {
	public static boolean isInDictionary(String area){
		if (area.equals(RoadAndTransportType.transport.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.bus.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.hole.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.traffic.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.parking.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.lorry.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.truck.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.car.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.motorbike.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.bike.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.rickshaw.toString())) {
			return true;
		}else if (area.equals(RoadAndTransportType.cycle.toString())) {
			return true;
		}
		return false;
	}
	public enum RoadAndTransportType {
		transport, bus, hole, traffic,  parking, lorry, truck,  car, motorbike, bike, rickshaw,cycle;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "road";
	}
}
