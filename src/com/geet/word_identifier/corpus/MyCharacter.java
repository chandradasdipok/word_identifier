package com.geet.word_identifier.corpus;

public class MyCharacter{
	private MyCharacter(){}
	private  char ch;
	private int charFrequency=0;
	
	public char getCh() {
		return ch;
	}
	public void setCh(char ch) {
		this.ch = ch;
	}
	public int getCharFrequency() {
		return charFrequency;
	}
	public void setCharFrequency(int charFrequency) {
		this.charFrequency = charFrequency;
	}
	public boolean isSame(MyCharacter character){
		if (ch == character.ch) {
			return true;
		} 
		return false;
	}
	
	public static class Builder{
		private  char ch;
		private int charFrequency=0;
		
		public Builder character(char ch){
			this.ch = ch;
			return this;
		}
		public Builder charFrequency(int frequency){
			this.charFrequency = frequency;
			return this;
		}
		public MyCharacter build(){
			MyCharacter character = new MyCharacter();
			character.setCh(ch);
			character.setCharFrequency(charFrequency);
			return character;
		}
	}
	
	@Override
	public String toString() {
		return "[ "+ch+","+charFrequency+"] ";
	}
	
}