package com.geet.word_identifier.corpus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SimpleWord implements Comparable<SimpleWord>, Serializable{
	private  String wordInString;
	private double score;
	private SimpleWord(){};
	public static class Builder{
		private  String wordInString="";
		private double score=0.0;
		
		public Builder word(String word){
			word.toLowerCase();
			wordInString = word;
			return this;
		}
		public Builder score(double score){
			this.score = score;
			return this;
		}
		public SimpleWord build(){
			SimpleWord simpleWord =  new SimpleWord();
			simpleWord.setWordInString(wordInString);
			simpleWord.setScore(score);
			return simpleWord;
		}	
	}
	public String getWordInString() {
		return wordInString;
	}
	public void setWordInString(String wordInString) {
		this.wordInString = wordInString;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	
	public boolean isSameTerm(SimpleWord simpleWord){
		if (getWordInString().equals(simpleWord.getWordInString())) {
			return true;
		}
		return false;
	}
	public List<MyCharacter> getCharacters() {
		List<MyCharacter> characters = new ArrayList<MyCharacter>();
		for (int i=0; i< wordInString.length();i++) {
			char ch = wordInString.charAt(i);
			ch = java.lang.Character.toLowerCase(ch);
			MyCharacter candidateCharacter = new MyCharacter.Builder().character(ch).charFrequency(1).build();
			int pass = -1;
			for (int j = 0; j< characters.size(); j++) {
				if (characters.get(j).isSame(candidateCharacter)) {
					pass = j;
					characters.get(j).setCharFrequency(characters.get(j).getCharFrequency()+1);
					continue;
				}
			}
			if (pass == -1) {
				characters.add(candidateCharacter);
			}
		}
		return characters;
	}
	public double getCharacterFrequency(char ch){
		double tf = 0.0;
		for (MyCharacter character : getCharacters()) {
			if (character.isSame(new MyCharacter.Builder().character(ch).build())) {
				return character.getCharFrequency();
			}
		}
		return tf;
	}
	@Override
	public int compareTo(SimpleWord o) {
		return Double.compare(score, o.getScore());
	}
}
