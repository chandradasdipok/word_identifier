package com.geet.word_identifier.indexing_vsm;

import java.io.Serializable;
import java.util.List;

import com.geet.word_identifier.corpus.SimpleWord;


public class VectorSpaceMatrix implements Serializable{
	private List<SimpleWord> words;
	private List<Character> chars;
	private double [][]TERM_DOCUMENT_MATRIX;
	private double [] df;
		
	public List<SimpleWord> getWords() {
		return words;
	}
	public void setWords(List<SimpleWord> words) {
		this.words = words;
	}
	public List<Character> getChars() {
		return chars;
	}
	public void setChars(List<Character> chars) {
		this.chars = chars;
	}
	public double[][] getTERM_DOCUMENT_MATRIX() {
		return TERM_DOCUMENT_MATRIX;
	}
	public void setTERM_DOCUMENT_MATRIX(double[][] tERM_DOCUMENT_MATRIX) {
		TERM_DOCUMENT_MATRIX = tERM_DOCUMENT_MATRIX;
	}
	public double[] getDf() {
		return df;
	}
	public void setDf(double[] df) {
		this.df = df;
	}
	public VectorSpaceMatrix(List<SimpleWord> words,
			List<Character> chars, double[][] tERM_DOCUMENT_MATRIX, double[] df) {
		super();
		this.words = words;
		this.chars = chars;
		TERM_DOCUMENT_MATRIX = tERM_DOCUMENT_MATRIX;
		this.df = df;
	}
	@Override
	public String toString() {
		String toString ="";
		for (int i = 0; i < TERM_DOCUMENT_MATRIX.length; i++) {
			for (int j = 0; j < TERM_DOCUMENT_MATRIX[i].length; j++) {
				toString +=(TERM_DOCUMENT_MATRIX[i][j]+" ");
			}
			toString+="\n";
		}
		return toString;
	}
	
}