package com.geet.word_identifier.indexing_vsm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.geet.word_identifier.corpus.MyCharacter;
import com.geet.word_identifier.corpus.SimpleWord;

public class VectorSpaceModel implements Serializable{
	
	private List<SimpleWord> words = new ArrayList<SimpleWord>();
	private List<Character> chars = new ArrayList<Character>();
	private double [][]TERM_DOCUMENT_MATRIX;
	private double [] df;
	private int totalChars=0;
	private int totalTerms=0;
	private double MINIMUM_SCORE = 0.70;
	
	public VectorSpaceModel(List<SimpleWord> wordList) {
		words = wordList;
		chars = getCharacters();
		totalChars = chars.size();
		totalTerms = words.size();
		df = new double[totalChars];
		TERM_DOCUMENT_MATRIX = new double[totalChars][totalTerms];
		df = new double [totalChars];
		System.out.println("Chars "+totalChars+" Terms "+totalTerms);
		setTERM_DOCUMENT_MATRIX(chars, words);
	}
	public VectorSpaceModel(VectorSpaceMatrix vectorSpaceMatrix){
		for (int i = 0; i < vectorSpaceMatrix.getWords().size(); i++) {
			SimpleWord simpleWord = new SimpleWord.Builder().word(vectorSpaceMatrix.getWords().get(i).getWordInString()).score(vectorSpaceMatrix.getWords().get(i).getScore())
					.build();
			words.add(simpleWord);
		}
		chars = vectorSpaceMatrix.getChars();
		TERM_DOCUMENT_MATRIX = vectorSpaceMatrix.getTERM_DOCUMENT_MATRIX();
		df = vectorSpaceMatrix.getDf();
	}
	public VectorSpaceMatrix getVectorSpaceMatrix(){
		List<SimpleWord> simpleWords = new ArrayList<SimpleWord >();
		for (int i = 0; i < words.size(); i++) {
			SimpleWord simpleWord = new SimpleWord.Builder().word(words.get(i).getWordInString()).score(words.get(i).getScore())
					.build();
			simpleWords.add(simpleWord);
		}
		return new VectorSpaceMatrix(simpleWords,chars,TERM_DOCUMENT_MATRIX, df);
	}
	public void setTERM_DOCUMENT_MATRIX(List<Character> chars,List<SimpleWord>words){
		/* dont calculate idf from terms rather compute it from matrix*/
		System.out.println("Character-Term Matrix getting...");
		for (int i = 0; i < totalChars; i++) {
			// df
			df[i] = 0;
			System.out.print(chars.get(i)+":");
			for (int j = 0; j < totalTerms; j++) {
				double tf = words.get(j).getCharacterFrequency(chars.get(i));
				if (tf !=0.0) {
						df[i]++;
				}
				TERM_DOCUMENT_MATRIX[i][j]= tf;
				System.out.print(TERM_DOCUMENT_MATRIX[i][j]+" ");
			}
			System.out.println();
		}
	}

	public List<SimpleWord> search(){
		List<SimpleWord>lsiDocuments = new ArrayList<SimpleWord>();	
		for (int i = 1; i < words.size(); i++) {
				double dotProduct = 0.0;
				double scalarOne = 0.0;
				double scalarTwo = 0.0;
				for (int j = 0; j < chars.size(); j++) {
					dotProduct += TERM_DOCUMENT_MATRIX[j][0] *(1 + (Math.log10((double)(words.size())/(df[j]))/Math.log10(2.0)))* TERM_DOCUMENT_MATRIX[j][i] * (1 + (Math.log10((double)(words.size())/(df[j]))/Math.log10(2.0)));
					scalarOne += TERM_DOCUMENT_MATRIX[j][0] * (1 + (Math.log10((double)(words.size())/(df[j]))/Math.log10(2.0)))* TERM_DOCUMENT_MATRIX[j][0] * (1 + (Math.log10((double)(words.size())/(df[j]))/Math.log10(2.0)));
					scalarTwo += TERM_DOCUMENT_MATRIX[j][i] * (1 + (Math.log10((double)(words.size())/(df[j]))/Math.log10(2.0)))* TERM_DOCUMENT_MATRIX[j][i]*(1 + (Math.log10((double)(words.size())/(df[j]))/Math.log10(2.0)));
				}
				words.get(i).setScore( (dotProduct)/(Math.sqrt(scalarOne)*Math.sqrt(scalarTwo)));
				if (words.get(i).getScore() > 0) {
					lsiDocuments.add((SimpleWord) words.get(i));
				}
		}
		return lsiDocuments;
	}
	public List<SimpleWord> searchWithQueryVector(Query query){
		int totalDocument = words.size() + 1 ;
		List<SimpleWord>lsiDocuments = new ArrayList<SimpleWord>();	
		for (int i = 0; i < words.size(); i++) {
				double dotProduct = 0.0;
				double scalarOne = 0.0;
				double scalarTwo = 0.0;
				for (int j = 0; j < chars.size(); j++) {
					double tempDf = df[j];
					if (tempDf != 0) {
						if (query.getVectorInVectorSpaceModel()[j] != 0) {
							tempDf =  1 + ((Math.log10((double)(totalDocument)/(tempDf+1)))/Math.log10(2.0));
						}
						else{
							tempDf =  1 + ((Math.log10((double)(totalDocument)/(tempDf)))/Math.log10(2.0));
						}
						dotProduct +=( query.getVectorInVectorSpaceModel()[j] * tempDf )* (TERM_DOCUMENT_MATRIX[j][i]* tempDf);
						scalarOne += (query.getVectorInVectorSpaceModel()[j] * tempDf)* (query.getVectorInVectorSpaceModel()[j] * tempDf);
						scalarTwo += (TERM_DOCUMENT_MATRIX[j][i] * tempDf)* (TERM_DOCUMENT_MATRIX[j][i]* tempDf);						
					}
				}
				for (int j = 0; j < query.getVectorInExtendedVectorSpaceModel().length; j++) {
				//	System.out.println(j);
					double tempDf =  1 + (Math.log10((double)(totalDocument)/(1))/Math.log10(2.0));
					scalarOne += (query.getVectorInExtendedVectorSpaceModel()[j] * tempDf)* (query.getVectorInExtendedVectorSpaceModel()[j]* tempDf);
				}
				if (scalarOne != 0 && scalarTwo != 0) {
					words.get(i).setScore((dotProduct)/(Math.sqrt(scalarOne)*Math.sqrt(scalarTwo)));
				//	System.out.println(words.get(i).getWordInString()+","+words.get(i).getScore());
					if (words.get(i).getScore() >= MINIMUM_SCORE) {
						lsiDocuments.add(words.get(i));
					}
				}
		}
		Collections.sort(lsiDocuments);
		Collections.reverse(lsiDocuments);
		return lsiDocuments;
	}
	
	public Query getQuery(SimpleWord simpleWord){
		Query query = new Query();
		System.out.println(simpleWord.getCharacters().toString());
		query.setVectorInVectorSpaceModel(new double[chars.size()]); 
		List<com.geet.word_identifier.corpus.MyCharacter> extensionChars = new ArrayList<com.geet.word_identifier.corpus.MyCharacter>();
		for (com.geet.word_identifier.corpus.MyCharacter character : simpleWord.getCharacters()) {
			int flag =0;
			for (int i = 0; i < query.getVectorInVectorSpaceModel().length; i++) {
				if (character.isSame(new com.geet.word_identifier.corpus.MyCharacter.Builder().character(chars.get(i)).build())) {
					query.getVectorInVectorSpaceModel()[i] = character.getCharFrequency();
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				System.out.println("Not in term_document_matrix"+character.getCh());
				extensionChars.add(character);
			}
		}
		query.setVectorInExtendedVectorSpaceModel(new double[extensionChars.size()]);
		for (int i = 0; i < extensionChars.size(); i++) {
			query.getVectorInExtendedVectorSpaceModel()[i] = extensionChars.get(i).getCharFrequency();
		}
		return query;
	}
	public List<Character> getCharacters(){
		Set<Character> characters = new HashSet<Character>();
		for (SimpleWord	 simpleWord : words) {
			Set<Character> candidateSet = new HashSet<Character>();
			for (MyCharacter myCharacter : simpleWord.getCharacters()) {
				candidateSet.add(myCharacter.getCh());
			}
			characters.addAll(candidateSet);
		}
		return new ArrayList<>(characters);
	}
	/**
	 * 
	 * @return
	 */
	/*public String TERM_DOCUMENT_MATRIX_TO_STRING(){
		String text="";
		char[] chars= getCharacters();
		String[] documents = getDOCS();
		double [][] TERM_DOCUMENT_MATRIX = getTERM_DOCUMENT_MATRIX();
		text += terms.length+"X"+documents.length+"\n";
		for (int i = 0; i < terms.length; i++) {
			text += terms[i]+" ";
			for (int j = 0; j < documents.length; j++) {
				text += TERM_DOCUMENT_MATRIX[i][j]+",";
			}
			text += "\n";
		}
		return text;
	}*/
	
}