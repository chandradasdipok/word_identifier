package com.geet.word_identifier.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;

import com.geet.word_identifier.model.Area;
import com.geet.word_identifier.model.Problem;
import com.geet.word_identifier.model.ProblemTrain;
import com.geet.word_identifier.model.RoadAndTransport;
import com.geet.word_identifier.model.WaterAndSupply;
import com.geet.word_identifier.searching.Run;

public class ProblemFinderUI extends JFrame{
	String selectedArea="",selectedType="";
	String [] areas  = new Area().getAreas();
	String [] types = {new RoadAndTransport().toString().toUpperCase(),new WaterAndSupply().toString().toUpperCase(),"others".toUpperCase()};
	List<Problem> allProblems,refinedProblems;
	DefaultListModel<Problem> listModel = new DefaultListModel<Problem>();
	JList problemList = new JList(listModel);
	JPanel problemPanel;
	public ProblemFinderUI() {
		super("Problem Finader");
		setLayout(null);
		JPanel comboBoxPanel = new JPanel();
		comboBoxPanel.setLayout(new FlowLayout());
		final JComboBox areaComboBox = new JComboBox(areas);
		areaComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int index = areaComboBox.getSelectedIndex();
				selectedArea = areas[index];
				setRefinedProblemsOnAreaAndType(selectedArea.toLowerCase(), selectedType.toLowerCase());
			}
		});
		comboBoxPanel.add(areaComboBox);
		final JComboBox typeComboBox = new JComboBox(types);
		typeComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int index = typeComboBox.getSelectedIndex();
				selectedType = types[index];
				setRefinedProblemsOnAreaAndType(selectedArea.toLowerCase(), selectedType.toLowerCase());
			}
		});
		comboBoxPanel.add(typeComboBox);
		comboBoxPanel.setBounds(10, 10, 700, 50);
		add(comboBoxPanel);
		allProblems = new ArrayList<Problem>();
		try {
			allProblems = Run.readProblemsFromTXTAndPredictWithVSM(("src/res/problems.txt"));
		} catch (Exception e) {
			e.printStackTrace();
			allProblems = new ArrayList<>();
		}
		refinedProblems = allProblems;
		showProblemListPanel();
		showFrame();
	}
	
	private void setRefinedProblemsOnAreaAndType(String area,String type){
		refinedProblems = new ArrayList<Problem>();
		if (type.equals("others")) {
			type ="";
		}
		if (area.equals("others")) {
			area = "";
		}
		for (Problem problem : allProblems) {
			if (problem.getArea().equals(area) && problem.getType().equals(type)) {
				refinedProblems.add(problem);
			}
		}
		updateList(refinedProblems);
	}
	private void showProblemListPanel(){		
		problemList = new JList(listModel);
		problemList.setCellRenderer(new TextAreaListItem(5, 20));
		for (Problem problem : refinedProblems) {
			listModel.addElement(problem);
		}
		JScrollPane scrollPane =new JScrollPane(problemList);
		scrollPane.setBounds(20, 60, 700,500);
		add(scrollPane);
	}
	private void showFrame(){
		setSize(800, 600);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new ProblemFinderUI();
	}
	
	
	private class TextAreaListItem extends JTextArea implements ListCellRenderer{
		protected TextAreaListItem(int rows, int cols){
			super(rows, cols);
	         setBorder(BorderFactory.createLineBorder(Color.blue));
		}
		@Override
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			Problem problem = (Problem) value;
			setText(problem.getDescription());
			return this;
		}
	}
	public void updateList(List<Problem> problems){
		listModel = new DefaultListModel<Problem>();
		for (Problem problem : problems) {
			listModel.addElement(problem);
		}	
		problemList.setModel(listModel);
	}
	
}
